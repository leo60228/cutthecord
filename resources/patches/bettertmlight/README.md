## DisTok CutTheCord: BetterTM Patch for Light Theme

This patch replaces the :tm: emoji and :registered: emojis with rendered unicode variants, making them look slightly better.

#### Conflicts with
- bettertm

